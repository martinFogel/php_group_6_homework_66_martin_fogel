<?php
$goals = [3, 5, 4, 1, 4, 7, 2, 9, 5, 6, 1, 8, 6, 2, 1, 8, 6, 2, 3, 8];
$losses = [2, 2, 4, 2, 6, 8, 2, 3, 4, 9, 2, 3, 4, 6, 2, 9, 2, 4, 5, 3];

$counter = 1;

for ($i = 0; $i < count($goals); $i++) {
    if ($goals[$i] == $losses[$i]) {
        print "{$counter} game was draw\n";
    } elseif ($goals[$i] < $losses[$i]) {
        print "{$counter} game team has lost\n";
    } elseif ($goals[$i] > $losses[$i]) {
        print "{$counter} game team has won\n";
    }
    $counter++;
}