<?php

print "Enter size of array: ";
$N = trim(fgets(STDIN));
$array = [];
$bool = true;
print "Enter array:\n";
for ($i = 0; $i < $N; $i++) {
    $array[] = trim(fgets(STDIN));
}

$d = $array[1] - $array[0];

for ($i = 1; $i < count($array) - 1; $i++) {
    if ($array[$i + 1] - $array[$i] != $d) {
        $bool = false;
        print "There is no arithmetic progression\n";
        return null;
    }
}
print "There is arithmetic progression.
Difference of arithmetic progression: {$d}\n";



