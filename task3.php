<?php
print "Enter first word: ";
$str_1 = trim(fgets(STDIN));
print "Enter second word: ";
$str_2 = trim(fgets(STDIN));
$arr1 = str_split($str_1);
$arr2 = str_split($str_2);
$counter = 0;

for ($i = 0; $i < count($arr1); $i++) {
    if ($arr1[$i] == $arr2[$i]) {
        $counter++;
    }
}
print "Quantity of matched letters from beginning: {$counter}\n";