<?php
$heights = [178, 165, 170, 172];
$students = ['John', 'Mike', 'Wes', 'Chris'];

$max_height = 0;
$name = '';

for ($i = 0; $i < count($heights); $i++) {
    if ($heights[$i] > $max_height) {
        $max_height = $heights[$i];
        $name = $students[$i];
    }
}

print "\nThe tallest man in the class is {$name} with height {$max_height}!\n\n";
