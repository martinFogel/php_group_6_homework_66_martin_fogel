<?php

$array = [1, 5, 2, 2, 7, 8, 1];

$max = 0;
$counter = 0;

for ($i = 0; $i < count($array); $i++) {
    if ($array[$i] > $max) {
        $max = $array[$i];
    }
}
for ($i = 0; $i < count($array); $i++) {
    if ($array[$i] == $max) {
        break;
    }
    $counter++;
}
print "Max: {$max}. Quantity before max: {$counter}\n";
